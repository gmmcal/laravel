<?php

class Book extends Eloquent {

	/**
	 * The name of the table associated with the model.
	 *
	 * @var string
	 */
	public static $table = 'books';

	/**
	 * Indicates if the model has update and creation timestamps.
	 *
	 * @var bool
	 */
	public static $timestamps = true;

	/**
	 * Establish the relationship between a book and a publisher.
	 *
	 * @return Laravel\Database\Eloquent\Relationships\Belongs_To
	 */
	public function publisher()
	{
		return $this->belongs_to('Publisher');
	}

	/**
	 * Establish the relationship between a book and a author.
	 *
	 * @return Laravel\Database\Eloquent\Relationships\Belongs_To
	 */
	public function author()
	{
		return $this->belongs_to('Author');
	}
}