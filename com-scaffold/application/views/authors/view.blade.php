<div class="span16">
	<ul class="breadcrumb span6">
		<li>
			<a href="{{URL::to('authors')}}">Authors</a> <span class="divider">/</span>
		</li>
		<li class="active">Viewing Author</li>
	</ul>
</div>

<div class="span16">
<p>
	<strong>Name:</strong>
	{{$author->name}}
</p>

<p><a href="{{URL::to('authors/edit/'.$author->id)}}">Edit</a> | <a href="{{URL::to('authors/delete/'.$author->id)}}" onclick="return confirm('Are you sure?')">Delete</a></p>
<h2>Books</h2>

@if(count($author->books) == 0)
	<p>No books.</p>
@else
	<table>
		<thead>
			<th>Publisher</th>
			<th>Name</th>
			<th>Description</th>
			<th>Created At</th>
			<th>Updated At</th>
			<th></th>
		</thead>

		<tbody>
			@foreach($author->books as $book)
				<tr>
					<td>{{$book->publisher->name}}</td>
					<td>{{$book->name}}</td>
					<td>{{$book->description}}</td>
					<td>{{$book->created_at}}</td>
					<td>{{$book->updated_at}}</td>
					<td><a href="{{URL::to('books/view/'.$book->id)}}">View</a> <a href="{{URL::to('books/edit/'.$book->id)}}">Edit</a> <a href="{{URL::to('books/delete/'.$book->id)}}">Delete</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif

<p><a class="btn success" href="{{URL::to('books/create/')}}">Create new book</a></p>
