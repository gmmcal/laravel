<?php

class Create_Books_Table {

	/**
	 * Make changes to the database.
	 *
	 * @return void
	 */
	public function up()
	{	
		Schema::create('books', function($table)
		{
			$table->increments('id');

			$table->integer('publisher_id');
			$table->integer('author_id');
			$table->string('name');
			$table->text('description');

			$table->timestamps();
		});
	}

	/**
	 * Revert the changes to the database.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('books');
	}

}