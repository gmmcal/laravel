@if(count($books) == 0)
	<p>No books.</p>
@else
	<table>
		<thead>
			<tr>
				<th>Author</th>
				<th>Publisher</th>
				<th>Name</th>
				<th>Description</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach($books as $book)
				<tr>
					<td><a href="{{URL::to('authors/view/'.$book->id)}}">{{$book->author->name}}</a></td>
					<td><a href="{{URL::to('publishers/view/'.$book->id)}}">{{$book->publisher->name}}</a></td>
					<td>{{$book->name}}</td>
					<td>{{$book->description}}</td>
					<td>
						<a href="{{URL::to('books/view/'.$book->id)}}">View</a>
						<a href="{{URL::to('books/edit/'.$book->id)}}">Edit</a>
						<a href="{{URL::to('books/delete/'.$book->id)}}" onclick="return confirm('Are you sure?')">Delete</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif

<p><a class="btn success" href="{{URL::to('books/create')}}">Create new Book</a></p>