<div class="span16">
	<ul class="breadcrumb span6">
    <li>
      <a href="{{URL::to('publishers')}}">Publishers</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="{{URL::to('authors')}}">Authors</a> <span class="divider">/</span>
    </li>
		<li>
			<a href="{{URL::to('books')}}">Books</a> <span class="divider">/</span>
		</li>
		<li class="active">New Book</li>
	</ul>
</div>

{{Form::open(null, 'post', array('class' => 'form-stacked span16'))}}
	<fieldset>
		<div class="clearfix">
			{{Form::label('author_id', 'Author Id')}}

			<div class="input">
				{{Form::text('author_id', Input::old('author_id', $author_id), array('class' => 'span6'))}}
			</div>
		</div>
		<div class="clearfix">
			{{Form::label('publisher_id', 'Publisher Id')}}

			<div class="input">
				{{Form::text('publisher_id', Input::old('publisher_id', $publisher_id), array('class' => 'span6'))}}
			</div>
		</div>
		<div class="clearfix">
			{{Form::label('name', 'Name')}}

			<div class="input">
				{{Form::text('name', Input::old('name'), array('class' => 'span6'))}}
			</div>
		</div>
		<div class="clearfix">
			{{Form::label('description', 'Description')}}

			<div class="input">
				{{Form::textarea('description', Input::old('description'), array('class' => 'span10'))}}
			</div>
		</div>

		<div class="actions">
			{{Form::submit('Save', array('class' => 'btn primary'))}}

			or <a href="{{URL::to(Request::referrer())}}">Cancel</a>
		</div>
	</fieldset>
{{Form::close()}}