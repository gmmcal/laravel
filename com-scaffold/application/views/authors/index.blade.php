@if(count($authors) == 0)
	<p>No authors.</p>
@else
	<table>
		<thead>
			<tr>
				<th>Name</th>
				<th>Books</th>
				<th></th>
			</tr>
		</thead>

		<tbody>
			@foreach($authors as $author)
				<tr>
					<td>{{$author->name}}</td>
					<td>{{count($author->books)}}</td>
					<td>
						<a href="{{URL::to('authors/view/'.$author->id)}}">View</a>
						<a href="{{URL::to('authors/edit/'.$author->id)}}">Edit</a>
						<a href="{{URL::to('authors/delete/'.$author->id)}}" onclick="return confirm('Are you sure?')">Delete</a>
					</td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif

<p><a class="btn success" href="{{URL::to('authors/create')}}">Create new Author</a></p>