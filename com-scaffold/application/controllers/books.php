<?php

class Books_Controller extends Base_Controller {

	/**
	 * The layout being used by the controller.
	 *
	 * @var string
	 */
	public $layout = 'layouts.scaffold';

	/**
	 * Indicates if the controller uses RESTful routing.
	 *
	 * @var bool
	 */
	public $restful = true;

	/**
	 * View all of the books.
	 *
	 * @return void
	 */
	public function get_index()
	{
		$books = Book::with(array('publisher', 'author'))->get();

		$this->layout->title   = 'Books';
		$this->layout->content = View::make('books.index')->with('books', $books);
	}

	/**
	 * Show the form to create a new book.
	 *
	 * @return void
	 */
	public function get_create($publisher_id = null, $author_id = null)
	{
		$this->layout->title   = 'New Book';
		$this->layout->content = View::make('books.create', array(
									'publisher_id' => $publisher_id,
									'author_id' => $author_id,
								));
	}

	/**
	 * Create a new book.
	 *
	 * @return Response
	 */
	public function post_create()
	{
		$validation = Validator::make(Input::all(), array(
			'author_id' => array('required', 'integer'),
			'publisher_id' => array('required', 'integer'),
			'name' => array('required'),
			'description' => array('required'),
		));

		if($validation->valid())
		{
			$book = new Book;

			$book->author_id = Input::get('author_id');
			$book->publisher_id = Input::get('publisher_id');
			$book->name = Input::get('name');
			$book->description = Input::get('description');

			$book->save();

			Session::flash('message', 'Added book #'.$book->id);

			return Redirect::to('books');
		}

		else
		{
			return Redirect::to('books/create')
					->with_errors($validation->errors)
					->with_input();
		}
	}

	/**
	 * View a specific book.
	 *
	 * @param  int   $id
	 * @return void
	 */
	public function get_view($id)
	{
		$book = Book::with(array('publisher', 'author'))->find($id);

		if(is_null($book))
		{
			return Redirect::to('books');
		}

		$this->layout->title   = 'Viewing Book #'.$id;
		$this->layout->content = View::make('books.view')->with('book', $book);
	}

	/**
	 * Show the form to edit a specific book.
	 *
	 * @param  int   $id
	 * @return void
	 */
	public function get_edit($id)
	{
		$book = Book::find($id);

		if(is_null($book))
		{
			return Redirect::to('books');
		}

		$this->layout->title   = 'Editing Book';
		$this->layout->content = View::make('books.edit')->with('book', $book);
	}

	/**
	 * Edit a specific book.
	 *
	 * @param  int       $id
	 * @return Response
	 */
	public function post_edit($id)
	{
		$validation = Validator::make(Input::all(), array(
			'author_id' => array('required', 'integer'),
			'publisher_id' => array('required', 'integer'),
			'name' => array('required'),
			'description' => array('required'),
		));

		if($validation->valid())
		{
			$book = Book::find($id);

			if(is_null($book))
			{
				return Redirect::to('books');
			}

			$book->author_id = Input::get('author_id');
			$book->publisher_id = Input::get('publisher_id');
			$book->name = Input::get('name');
			$book->description = Input::get('description');

			$book->save();

			Session::flash('message', 'Updated book #'.$book->id);

			return Redirect::to('books');
		}

		else
		{
			return Redirect::to('books/edit/'.$id)
					->with_errors($validation->errors)
					->with_input();
		}
	}

	/**
	 * Delete a specific book.
	 *
	 * @param  int       $id
	 * @return Response
	 */
	public function get_delete($id)
	{
		$book = Book::find($id);

		if( ! is_null($book))
		{
			$book->delete();

			Session::flash('message', 'Deleted book #'.$book->id);
		}

		return Redirect::to('books');
	}
}