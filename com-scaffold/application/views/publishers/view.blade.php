<div class="span16">
	<ul class="breadcrumb span6">
		<li>
			<a href="{{URL::to('publishers')}}">Publishers</a> <span class="divider">/</span>
		</li>
		<li class="active">Viewing Publisher</li>
	</ul>
</div>

<div class="span16">
<p>
	<strong>Name:</strong>
	{{$publisher->name}}
</p>

<p><a href="{{URL::to('publishers/edit/'.$publisher->id)}}">Edit</a> | <a href="{{URL::to('publishers/delete/'.$publisher->id)}}" onclick="return confirm('Are you sure?')">Delete</a></p>
<h2>Books</h2>

@if(count($publisher->books) == 0)
	<p>No books.</p>
@else
	<table>
		<thead>
			<th>Author</th>
			<th>Name</th>
			<th>Description</th>
			<th>Created At</th>
			<th>Updated At</th>
			<th></th>
		</thead>

		<tbody>
			@foreach($publisher->books as $book)
				<tr>
					<td>{{$book->author->name}}</td>
					<td>{{$book->name}}</td>
					<td>{{$book->description}}</td>
					<td>{{$book->created_at}}</td>
					<td>{{$book->updated_at}}</td>
					<td><a href="{{URL::to('books/view/'.$book->id)}}">View</a> <a href="{{URL::to('books/edit/'.$book->id)}}">Edit</a> <a href="{{URL::to('books/delete/'.$book->id)}}">Delete</a></td>
				</tr>
			@endforeach
		</tbody>
	</table>
@endif

<p><a class="btn success" href="{{URL::to('books/create/')}}">Create new book</a></p>
