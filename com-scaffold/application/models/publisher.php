<?php

class Publisher extends Eloquent {

	/**
	 * The name of the table associated with the model.
	 *
	 * @var string
	 */
	public static $table = 'publishers';

	/**
	 * Indicates if the model has update and creation timestamps.
	 *
	 * @var bool
	 */
	public static $timestamps = true;

	/**
	 * Establish the relationship between a publisher and books.
	 *
	 * @return Laravel\Database\Eloquent\Relationships\Has_Many
	 */
	public function books()
	{
		return $this->has_many('Book');
	}
}