<div class="span16">
	<ul class="breadcrumb span6">
    <li>
      <a href="{{URL::to('publishers/view/'.$book->publisher->id)}}">Publisher</a> <span class="divider">/</span>
    </li>
    <li>
      <a href="{{URL::to('authors/view/'.$book->author->id)}}">Author</a> <span class="divider">/</span>
    </li>
		<li>
			<a href="{{URL::to('books')}}">Books</a> <span class="divider">/</span>
		</li>
		<li class="active">Viewing Book</li>
	</ul>
</div>

<div class="span16">
<p>
	<strong>Author:</strong>
	{{$book->author->name}}
</p>
<p>
	<strong>Publisher:</strong>
	{{$book->publisher->name}}
</p>
<p>
	<strong>Name:</strong>
	{{$book->name}}
</p>
<p>
	<strong>Description:</strong>
	{{$book->description}}
</p>

<p><a href="{{URL::to('books/edit/'.$book->id)}}">Edit</a> | <a href="{{URL::to('books/delete/'.$book->id)}}" onclick="return confirm('Are you sure?')">Delete</a></p>
